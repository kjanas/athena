################################################################################
# Package: GeoModelErrorHandler
################################################################################

# Declare the package name:
atlas_subdir( GeoModelErrorHandler )

# Install files from the package:
atlas_install_headers( GeoModelErrorHandler )
